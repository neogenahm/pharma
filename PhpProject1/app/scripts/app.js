"use strict";
  
var middleware_url = "http://192.168.1.100/PharmacyMiddleware/";

var app = angular.module("pharmacyApp", ["ngAnimate", 
    "ngCookies",
    "ngResource",
    "ngSanitize", 
    "ngTouch", 
    "ngMessages",
    "picardy.fontawesome",
    "ui.bootstrap",
    "ui.router",
    "ui.utils", 
    "angular-loading-bar",
    "angular-momentjs",
    "FBAngular", 
    "lazyModel",
    "toastr",
    "angularBootstrapNavTree",
    "oc.lazyLoad", 
    "ui.select", 
    "ui.tree", 
    "textAngular",
    "colorpicker.module", 
    "angularFileUpload", 
    "ngImgCrop", 
    "datatables", 
    "datatables.bootstrap", 
    "datatables.colreorder", 
    "datatables.colvis", 
    "datatables.tabletools", 
    "datatables.scroller", 
    "datatables.columnfilter", 
    "ui.grid", 
    "ui.grid.resizeColumns", 
    "ui.grid.edit", 
    "ui.grid.moveColumns", 
    "ngTable", 
    "smart-table", 
    "angular-flot", 
    "angular-rickshaw", 
    "easypiechart", 
    "uiGmapgoogle-maps", 
    "ui.calendar", 
    "ngTagsInput", 
    "pascalprecht.translate",
    "ngMaterial", 
    "localytics.directives", 
    "leaflet-directive", 
    "wu.masonry", 
    "ipsum", 
    "angular-intro", 
    "dragularModule"])
    .run(["$rootScope", "$state", "$stateParams", function($rootScope, $state, $stateParams) {
    $rootScope.$state = $state, 
        $rootScope.$stateParams = $stateParams,
        $rootScope.$on("$stateChangeSuccess", function(event, toState) {
            event.targetScope.$watch("$viewContentLoaded", function() {
                angular.element("html, body, #content").animate({
                    scrollTop: 0}, 200), 
                setTimeout(function() {
                    angular.element("#wrap").css("visibility", "visible"), 
                    angular.element(".dropdown").hasClass("open") || angular.element(".dropdown").find(">ul").slideUp()
                }, 200)
        }), $rootScope.containerClass = toState.containerClass
    })
}]).config(["uiSelectConfig", function(uiSelectConfig) {
    uiSelectConfig.theme = "bootstrap"
}]).config(["$translateProvider", function($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: "languages/",
        suffix: ".json"
    }), 
            $translateProvider.useLocalStorage(), 
            $translateProvider.preferredLanguage("en"),
            $translateProvider.useSanitizeValueStrategy(null)
}]).config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login"), 
    $stateProvider.state("app", {
        "abstract": !0,
        url: "/app",
        templateUrl: "views/tmpl/app.html"
    }).state("login", {
        url: "/login",
        controller: "LoginCtrl",
        templateUrl: "views/tmpl/login.html"
    }).state("app.dashboard", {
        url: "/dashboard",
        controller: "DashboardCtrl",
        templateUrl: "views/tmpl/dashboard.html",
        resolve: {
            plugins: ["$ocLazyLoad", function(a) {
                return a.load(["scripts/vendor/datatables/datatables.bootstrap.min.css", "scripts/vendor/datatables/datatables.bootstrap.min.css"])
            }]
        }
    }).state("app.register", {
        url: "/register",
        controller: "PharmacyCtrl",
        templateUrl: "views/tmpl/PharamcyUser/user_add.html"
    }).state("app.userList", {
        url: "/userList",
        controller: "PharmacyCtrl",
        templateUrl: "views/tmpl/PharamcyUser/user_list.html"
    }).state("forgetpass", {
        url: "/forgetPassword",
        controller: "ForgotPasswordCtrl",
        templateUrl: "views/tmpl/forgotpass.html"
    }).state("forgetPwdReset", {
        url: "/forgetPwdReset",
        controller: "ForgotPasswordCtrl",
        templateUrl: "views/tmpl/forgotPassReset.html"
    })
     

}]);

