'use strict';
app.controller("DashboardCtrl", ['$state', '$scope', '$http', '$rootScope', "DTOptionsBuilder", "DTColumnDefBuilder", '$interval', function ($state, $scope, $http, $rootScope, b, c, $interval) {
        //var dataJson =JSON.stringify(loginDetail);
        /*$scope.projects = [];
         
         $scope.ProjectProgressCtrl = function () {
         $http.post('http://localhost/IonicMiddleware/?type=getTableData',{headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
         ).success(function(data, status) {                      
         if (data.status == 1)
         {     
         console.log(status,data);
         $scope.projects = data; 
         }
         else
         {                                                     
         $scope.errors.push(data.msg);
         }
         });
         }*/
    }]).controller('ProjectProgressCtrl', ['$state', '$scope', '$http', '$rootScope', "DTOptionsBuilder", "DTColumnDefBuilder", '$interval', '$uibModal', '$log', function ($state, $scope, $http, $rootScope, b, c, $interval, $uibModal, $log) {

        $scope.http = middleware_url;

        //$scope.http = 'http://skaffpharmacy.com/IonicMiddleware/'; 

        $scope.showhideprop = false;
        $scope.showRefillReady = false;
        $scope.shownotification = false;
        $scope.userdata = {};
        $scope.getAllDatas = [];
        $scope.user_id = localStorage.getItem('id');

        $scope.NotificationList = function (status = '', type = '', requestType = '') {

            if (type == 'filter')
            {
                $scope.shownotification = false;
                localStorage.removeItem("currentSelectedRecord");
            }
            if (status)
            {
                localStorage.setItem("filterStatus", status);
                //NotificationDetail.statusID =  status;
            }

            if (requestType)
            {
                $scope.shownotification = false;
                localStorage.setItem("requestType", requestType);
                localStorage.removeItem("currentSelectedRecord");
            }

            var NotificationDetail = {
                type: 'getTableData',
                startDate: localStorage.getItem("filterStartDate"),
                endDate: localStorage.getItem("filterEndDate"),
                statusID: localStorage.getItem("filterStatus"),
                requestType: localStorage.getItem("requestType")
            };

            var dataJson = JSON.stringify(NotificationDetail);
            $http.post($scope.http, dataJson, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}})
                    .success(function (data) {
                        $scope.getAllDatas = data.response;
                        $scope.getAllDatas, $scope.dtOptions = b.newOptions().withBootstrap(), $scope.dtColumnDefs = [c.newColumnDef(0), c.newColumnDef(1), c.newColumnDef(2), c.newColumnDef(3), c.newColumnDef(4).notSortable()];

                        //set order counts

                        $scope.countTotalOrder = data.response.length;
                        $scope.countPendingOrder = data.orderCounts.countPending;
                        $scope.countAckOrder = data.orderCounts.countAck;
                        $scope.countReadyOrder = data.orderCounts.countReady;
                        setSelectedRow();

                    })
                    .error(function () {
                        $scope.data = "error in fetching data";
                    });
        }

        $scope.$on("orderlist", function (event) {
            $scope.NotificationList();
        });

        //it's use to display data transaction table from trasacation_Id wise
        $scope.disabled = true;
        $scope.getUserDetail = function (id, index = '') {

            //get the selected index of order table	
            //set it to localstorage for store default selected row
            if (index)
            {
                $scope.selectedRow = index;
                localStorage.setItem("currentSelectedRecord", index);
                $scope.messageName = "";
            }
            $scope.value = "";
            $scope.showhideprop = false;
            $scope.showRefillReady = false;
            $scope.shownotification = true;

            var userdetail = {type: 'getUserDetail', id: id};
            $http.post($scope.http, userdetail, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
            ).success(function (userdatas) {

                $scope.userdata = userdatas['0'];


                if ($scope.userdata.status_id == "4") {
                    document.getElementById('refillReadyId').disabled = true;
                } else if ($scope.userdata.status_id == "3") {
                    document.getElementById('refillReadyId').disabled = false;
                }


                if ($scope.userdata.status_id == "1") {
                    document.getElementById('refillReadyId').disabled = false;
                } else if ($scope.userdata.status_id == "4") {
                    document.getElementById('refillReadyId').disabled = true;
                }

                if ($scope.userdata.status_id == "3")
                    document.getElementById('modalCreateBtn').disabled = false;
                else if ($scope.userdata.status_id == "2") {
                    document.getElementById('modalCreateBtn').disabled = false;
                } else if ($scope.userdata.status_id == "4") {
                    document.getElementById('modalCreateBtn').disabled = true;
                }

                if ($scope.userdata.status_id == "3")
                    document.getElementById('refillReadyId').disabled = false;
                else if ($scope.userdata.status_id == "2") {
                    document.getElementById('refillReadyId').disabled = false;
                } else if ($scope.userdata.status_id == "4") {
                    document.getElementById('refillReadyId').disabled = true;
                }

                if ($scope.userdata.status_id == "3")
                    document.getElementById('modalCreateBtn').disabled = true;
                else if ($scope.userdata.status_id == "1") {
                    document.getElementById('modalCreateBtn').disabled = false;
                } else if ($scope.userdata.status_id == "4") {
                    document.getElementById('modalCreateBtn').disabled = true;
                }


                if ($scope.userdata.status_id == "2")
                    document.getElementById('modalCreateBtn').disabled = false;
                else if ($scope.userdata.status_id == "3") {
                    document.getElementById('modalCreateBtn').disabled = true;
                }

                var objDiv = document.getElementById("autoScroll");
                objDiv.scrollTop = objDiv.scrollHeight;
                return $scope.userdata;

            })
                    .error(function () {
                        $scope.data = "error in fetching user data";
                    });

            ////////////////////chat Message////////
            var Message = {type: 'ChatMessageList', id: id};

            $http.post($scope.http, Message, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}})
                    .success(function (data) {
                        $scope.MessageLists = data;
                        return $scope.MessageList;
                    })

            /////////////chat message End//////////
        }

        $scope.$on("userDetail", function (event, id) {
            $scope.getUserDetail(id);
        });

        //Send Acknoweledge to pass value and static Message 
        $scope.SendAcknoweledge = function () {
            $scope.showRefillReady = false;
            $scope.showhideprop = false;
            $scope.messageName = "We have received your order";
            $scope.value = "acknowledge";

        }

        //Send RefillReadyAck to pass value and static Message
        $scope.SendRefillReadyAck = function () {
            $scope.showhideprop = false;
            $scope.showRefillReady = true;
            $scope.messageName = "Your order is ready";
            $scope.value = "ready";

        }

        //Get Technician list 
        $scope.getTechnicianList = function () {

            var TechnicienList = {type: 'getTechnicienList'};

            $http.post($scope.http, TechnicienList, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}})
                    .success(function (data) {
                        $scope.items = data.arr;
                    })

        }
         $scope.myobj={};
        //Onchange get TechnicianName 
        $scope.getTechnicianName = function (name) {
            $scope.ErrorMsg = "";
            $scope.myobj.pinnumber="";
            
            delete $scope.pinnumber;
            //alert(name.id);
            if (name != null) {
                $scope.techName = name.id;
                document.getElementById('gettechnicianValue').disabled = true;
            } else {
                document.getElementById('gettechnicianValue').disabled = true;
            }

        }
        
        //OnChange function Technician name and pin number Check  
        $scope.technicianValueCheck = function (pin) {
            $scope.ErrorMsg = "";
            var pinlen = pin.length;
            var TechnicianName = $scope.techName;
            var textMessage = $scope.messageName;
            var transaction_id = $scope.userdata.id;
            
            //alert(TechnicianName);
            if (TechnicianName && pinlen == 4 && textMessage) {
         

                var checkTechnicianDetail = {type: 'technianValueCheck', TechnicianName: TechnicianName, pin: pin};

                $http.post($scope.http, checkTechnicianDetail, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}})
                        .success(function (data) {

                            if (data.status == 1) {
                    
                                document.getElementById('gettechnicianValue').disabled = false;
                                var notificationData = {type: 'RefillReadyAcknowledge', user_id: $scope.user_id, id: transaction_id, message: $scope.messageName, value: $scope.value,technician_id:TechnicianName};
                                $http.post($scope.http, notificationData, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
                                ).success(function (responseMain) {
                                    var pushData = {
                                        "tokens": [responseMain.token],
                                        "profile": "neogenrxtest",
                                        "notification": {
                                            "message": $scope.NotificationMsg
                                        }
                                    };

                                    $scope.messageName = "";
                                    $scope.pinnumber = "";
                                    $scope.techName = "";

                                    $http.post('https://api.ionic.io/push/notifications', pushData, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxOWFiZTQ2Zi03NTRkLTQxYTEtYTFhYS0zMWFmODQ4MmEwYTAifQ.M0p7WmqPwIKnMMBu0K1dz9v48IOgDLSM70kvc_LlFHk'}}
                                    ).success(function (response) {

                                        $scope.NotificationList();
                                        $scope.getTechnicianList();

                                    });

                                    $scope.getUserDetail(transaction_id);

                                })
                                        .error(function () {
                                            $scope.data = "error in fetching user data";
                                        });
                            } else
                            {
                                document.getElementById('gettechnicianValue').disabled = true;
                                
                                $scope.ErrorMsg = data.msg;
                                  
                            }
                        });
            } else if (TechnicianName && pinlen == 4) {
               
                var checkTechnicianDetail = {type: 'technianValueCheck', TechnicianName: TechnicianName, pin: pin,technician_id:TechnicianName};

                $http.post($scope.http, checkTechnicianDetail, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}})
                        .success(function (data) {

                            if (data.status == 1) {
                                
                                document.getElementById('gettechnicianValue').disabled = false;

                            } else
                            {
                                document.getElementById('gettechnicianValue').disabled = true;
                                $scope.ErrorMsg = data.msg;
                     
                            }
                        });
            }

        }
        //Refill Ready to Send Notification 
        $scope.NotificationMsg = "Message From NeogenRx";
        $scope.RefillReadyAck = function (id) {
            //alert($scope.techName);
            if ($scope.messageName)
            {

                var notificationData = {type: 'RefillReadyAcknowledge', user_id: $scope.user_id, id: id, message: $scope.messageName, value: $scope.value,technician_id:$scope.techName};
                $http.post($scope.http, notificationData, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
                ).success(function (responseMain) {
                    var pushData = {
                        "tokens": [responseMain.token],
                        "profile": "neogenrxtest",
                        "notification": {
                            "message": $scope.NotificationMsg
                        }
                    };
                    $http.post('https://api.ionic.io/push/notifications', pushData, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxOWFiZTQ2Zi03NTRkLTQxYTEtYTFhYS0zMWFmODQ4MmEwYTAifQ.M0p7WmqPwIKnMMBu0K1dz9v48IOgDLSM70kvc_LlFHk'}}
                    ).success(function (response) {
                        $scope.messageName = "";
                        $scope.NotificationList();
                        $scope.getTechnicianList();
                    });

                    $scope.getUserDetail(id);

                })
                        .error(function () {
                            $scope.data = "error in fetching user data";
                        });
            } else
            {
                //get the selected index of order table

                $scope.selectedRow = angular.element(document.querySelector(".selected:nth-child(1)")).children().first().attr('data-id');
                //To display validation message
                angular.element(document.querySelector(".notificationTextRequired")).show();
                //To make the border of text field red while message is empty
                angular.element(document.querySelector(".error")).css("border-color", "red");

                //Both of the above function will revert after 3 seconds of time
                setTimeout(function ()
                {
                    //make the border of text field white
                    angular.element(document.querySelector(".error")).css("border-color", "white");
                    //hide the error message                      
                    angular.element(document.querySelector(".notificationTextRequired")).hide();

                }, 3000);
            }
            setSelectedRow();
        }


        //notification  message from patient to pharmacy chat message list 

        $scope.notifyMessagePatient = function () {

            $scope.id = localStorage.getItem('id');

            $scope.notificationData = [];
            var notification = {type: 'ChatMessagePatientToPharmacy', user_id: $scope.id};
            $http.post($scope.http, notification, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
            ).success(function (response) {
                //console.log(response);
                $scope.notificationData = response;
                angular.forEach($scope.notificationData, function (value, key) {
                    $scope.NotifyMessageFromApp(value.transaction_id, value.name, value.contact, value.message, value.date_time);
                });

                if (response.length > 0)
                    $scope.getUserDetail(response[0].transaction_id);

            })
                    .error(function () {
                        $scope.data = "error in fetching user data";
                    });
        }


        if (localStorage.getItem('id') != null)
        {
            $interval($scope.notifyMessagePatient, 15000);
        }

        //it's use to dispaly data from  notification message  
        $scope.NotifyMessageFromApp = function (id, name, contact, message, date) {
            $scope.id = localStorage.getItem('id');

            if (!Notification) {
                alert('Desktop notifications not available in your browser. Try Chromium.');
                return;
            }

            if (Notification.permission !== "granted")
                Notification.requestPermission();
            else {
                var notification = new Notification('Message', {
                    icon: 'http://neogeninfotech.com/images/24.png',
                    body: "Name : " + name + "," + contact + "\nMesaage: " + message + "\nDate : " + date,
                    requireInteraction: true,
                    data: {
                        id: id
                    }
                });

                notification.onclick = function () {

                    notification.close();
                }
            }

        }

        //transaction dropdown code
        $scope.transactionStatus = function () {
            var type = {type: 'getTransactionStatus'};

            $http.post($scope.http, type, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}})
                    .success(function (response) {
                        $scope.transactionStatusResponse = response;

                    })
                    .error(function () {
                        $scope.data = "error in fetching data";
                    });
        }

        $scope.setSelectedStatus = function (status) {
            $scope.selectedStatus = status;
        }


        $scope.setRequestType = function (status) {
            $scope.requestType = status;
        }

        // default temp is false, once the init method called it get false and prevent the mentod to repeated calls
        var temp = true;
        var init = function () {

            if (temp)
            {

                // set default order Count
                $scope.countTotalOrder = 0;
                $scope.countPendingOrder = 0;
                $scope.countAckOrder = 0;
                $scope.countReadyOrder = 0;

                var id = localStorage.getItem('id');
                var token = localStorage.getItem('token');

                if (id == null || token == null)
                {

                    $state.go('login');
                }
                temp = false;
                localStorage.removeItem("currentSelectedRecord");
                localStorage.removeItem("filterStartDate");
                localStorage.removeItem("filterEndDate");
                localStorage.removeItem("filterStatus");
                localStorage.removeItem("requestType");


                $scope.selectedStatus = "Select Status";
                $scope.requestType = "Select Type";
            }
        };

        init();

        // To set the row selected in ordertable 
        var setSelectedRow = function () {
            //alert("default selected = "+localStorage.getItem("currentSelectedRecord"));
            $scope.selectedRow = localStorage.getItem("currentSelectedRecord");
        }

    }]);

	