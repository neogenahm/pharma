'use strict';
 
app.controller("DaterangepickerCtrl", ["$scope", "$moment", function(a, b) {
        a.endDate = b().subtract(29, "days").format("MMMM D, YYYY"), 
        a.startDate = b().format("MMMM D, YYYY"),
        a.rangeOptions = {
        ranges: {
            Today: [b(), b()],
            Yesterday: [b().subtract(1, "days"), b().subtract(1, "days")],
            "Last 7 Days": [b().subtract(6, "days"), b()],
            "Last 30 Days": [b().subtract(29, "days"), b()],
            "This Month": [b().startOf("month"), b().endOf("month")],
            "Last Month": [b().subtract(1, "month").startOf("month"), b().subtract(1, "month").endOf("month")]
        },
        opens: "left",
        startDate: b().subtract(29, "days"),
        endDate: b(),
        parentEl: "#content"
    }
    


    
}]);
