 'use strict';

 app.controller("ForgotPasswordCtrl", ["$scope","$http","$state", function($scope,$http,$state) {

 	$scope.http = middleware_url;

 	$scope.forgetPassword = function(){
 		
 		var forgetDetail = {
                email: $scope.user.email,
                type: 'forgetPassword'
            };

            $http.post($scope.http, forgetDetail, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
            ).success(function (data) {			
					if(data.status == 1){
						$scope.forgetpwd = true;
                    	$scope.forgorMsg = data.msg;          
					}else
                	{
                    	data.msg;
                	}
            });
 	}

 	$scope.forgetpwdReset = function(){

 		var forgetReset = {
                password: $scope.user.password,
                type: 'forgetPasswordReset'
            };

            $http.post($scope.http, forgetReset, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
            ).success(function (data) {	
            		alert(data.status);			
					if(data.status == 1){
						//$state.go("login");         
					}else
                	{
                    	//data.msg;
                	}
            });
 	}
}]);
