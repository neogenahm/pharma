'use strict';

app.controller("LoginCtrl", ["$scope", "$state", "$http", function ($scope, $state, $http) {
        $scope.errors = [];
        $scope.msgs = [];
		
        $scope.http = middleware_url;
         
    
        $scope.login = function () {
            
            var loginDetail = {
                email: $scope.user.email,
                password: $scope.user.password,
                type: 'logincheckAdmin'
            };
            
            $http.post($scope.http, loginDetail, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
            ).success(function (data) {	
                    	
					if(data.status == 1){
					localStorage.setItem('id', data.id);
					localStorage.setItem('token', data.token);	
			
                    $state.go("app.dashboard");
					}else
                	{
                        data.msg;
                	}
            });
        }


        var temp = true;
        var init = function () {
            
            var id = localStorage.getItem('id');
            var token = localStorage.getItem('token');
            if (id != null && token != null && temp)
            {
                temp = false;               
                $state.go('app.dashboard');
            }
        };
        
        init();
        
    }]);
