'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the minovateApp
 */
app
        .controller('MainCtrl', function ($scope, $http, $translate, $interval, $state,$rootScope) {
             $scope.http = middleware_url;
           
            //$scope.http = 'http://skaffpharmacy.com/IonicMiddleware/';

             $scope.logout = function () {
               
                 localStorage.removeItem('id');
                 localStorage.removeItem('token');
                 location.reload();
                 $state.go("login");
             }

            //new Notification display in Header 
            /*$scope.NewNotifyListHeader = function () {
                $scope.id = localStorage.getItem('id');
                $scope.newnotificationData = [];
                var notification = {type: 'getNotifyDataListHeader', user_id: $scope.id};
                $http.post($scope.http, notification, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
                ).success(function (response) {

                    $scope.newnotificationData = response;
                    $scope.newnotificationData.count = $scope.newnotificationData.length;

                })
                        .error(function () {
                            $scope.data = "error in fetching user data";
                        });
            }*/

            $scope.HeaderNotifyShow = function (id) {
                $scope.initRestId(id);
            }

            $scope.initRestId = function (id) {
                $scope.$broadcast("userDetail", id);
            }


            ///This function use to get notification dispaly
            $scope.notifyAjax = function () {

                $scope.id = localStorage.getItem('id');

                $scope.notificationData = [];
                var notification = {type: 'getNotifyData', user_id: $scope.id};
                $http.post($scope.http, notification, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
                ).success(function (response) {
                    
                    $scope.notificationData = response;
                    angular.forEach($scope.notificationData, function (value, key) {
                        $scope.notifyMe(value.id, value.first_name,value.middle_name,value.sur_name, value.contact, value.prescription_id,value.medicine_name,value.comment,value.create_on);
                    });

                    if (response.length > 0)
                    {
                        $scope.orderList();
                    }
                })
                        .error(function () {
                            $scope.data = "error in fetching user data";
                        });

            }

            // This function use get notification display in desktop
            $scope.AckMessage = "We Receive your order";
            $scope.NotificationMsg = "Message From NeogenRx";
            $scope.notifyMe = function (id, first_name,middle_name,sur_name, contact, prescription_id,medicine_name, comment,create_on) {
                
                $scope.id = localStorage.getItem('id');

                if (!Notification) {
                    alert('Desktop notifications not available in your browser. Try Chromium.');
                    return;
                }

                if (Notification.permission !== "granted")
                    Notification.requestPermission();
                else {
                     var notification = new Notification('Prescription Detail', {
                        icon: 'http://neogeninfotech.com/images/24.png',
                        body: "Name : " + first_name +" "+middle_name+" "+ sur_name+"," + contact + "\nRx No : " + prescription_id +"\nMedicine Name :"+ medicine_name +"\nComment: " + comment + "\nDate : " + create_on,
                        requireInteraction: true,
                        data: {
                            id: id
                        }
                    });

                    notification.onclick = function () {                        
                        var notificationData = {type: 'acknowledge', user_id: $scope.id, id: id, message: $scope.AckMessage};
                        $http.post($scope.http, notificationData, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
                        ).success(function (responseMain) {
                           
                            if (responseMain.status == 1) {

                                var pushData = {
                                    "tokens": [responseMain.token],
                                    "profile": "neogenrxtest",
                                    "notification": {
                                        "message": $scope.NotificationMsg
                                    }
                                };

                                $http.post('https://api.ionic.io/push/notifications', pushData, {headers: {'Content-Type': 'application/json', 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIxOWFiZTQ2Zi03NTRkLTQxYTEtYTFhYS0zMWFmODQ4MmEwYTAifQ.M0p7WmqPwIKnMMBu0K1dz9v48IOgDLSM70kvc_LlFHk'}}
                                ).success(function (response) {

                                    notification.close();
                                    $scope.orderList();
                                    $scope.initRestId(responseMain.transaction_id)
                                });
                            }
                            else {

                                notification.close();
                                alert(responseMain.msg);
                            }

                        })
                                .error(function () {
                                    $scope.data = "error in fetching user data";
                                });

                    };


                }
            }

            $scope.orderList = function () {
                $scope.$broadcast("orderlist");
            }

            if (localStorage.getItem('id') != null)
            {
                $interval($scope.notifyAjax, 10000);
            }

            $scope.SetSessionValue = function () {
                $scope.id = localStorage.getItem('id');
                $scope.token = localStorage.getItem('token');


                var sessiondata = {type: "getSessionValue", user_id: $scope.id, token: $scope.token};

                $http.post($scope.http, sessiondata, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
                ).success(function (response) {

                    if (response.status == 1) {

                    }
                    else {
                        localStorage.removeItem('id');
                        localStorage.removeItem('token');
                        $state.go("login");
                    }
                });

            }

             if (localStorage.getItem('id') != null)
            {
                $interval($scope.SetSessionValue, 5000);
            }

            $scope.getLoginUserDetail = function () {
                $scope.id = localStorage.getItem('id');

                var getUserdetail = {type: "getLoginUserDetail", user_id: $scope.id};

                $http.post($scope.http, getUserdetail, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
                ).success(function (response) {
                    $scope.userDetail = response['0'];

                });

            }


        });
