'use strict'; 

app.controller("NavCtrl", ["$scope", function($scope) {
    $scope.oneAtATime = false,
	$scope.status = {
        isFirstOpen: true,
        isSecondOpen: true,
        isThirdOpen: true
    }
}]);
