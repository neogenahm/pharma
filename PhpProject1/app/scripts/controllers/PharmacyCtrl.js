	
'use strict';

app.controller("PharmacyCtrl", ['$scope', '$http','$state','$rootScope', "DTOptionsBuilder", "DTColumnDefBuilder", function($scope, $http,$state,$rootScope,b,c) {
    //$scope.awesomeThings = ["HTML5 Boilerplate", "AngularJS", "Karma"]
	$scope.errors = [];
	 $scope.http = middleware_url;
	 // $scope.http = 'http://localhost/IonicMiddleware/';
	 //$scope.http = 'http://skaffpharmacy.com/IonicMiddleware/'; 
	//$scope.http = "http://neogeninfotech.com/IonicMiddleware/";
	$scope.list = function(){
	 var listData = {
                type: 'userList'				
            };
            var dataJson = JSON.stringify(listData);
		
		$scope.getAllDatas = [];
		$http.post($scope.http,dataJson,{headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}})
			.success(function(data){				
				$scope.getAllDatas = data
				//console.log($scope.getAllDatas);
				$scope.getAllDatas,$scope.dtOptions = b.newOptions().withBootstrap(), $scope.dtColumnDefs = [c.newColumnDef(0), c.newColumnDef(1), c.newColumnDef(2), c.newColumnDef(3), c.newColumnDef(4).notSortable()];				
				
			})
			.error(function() {
				$scope.data = "error in fetching data";
			});	
	
	
	}
	
	$scope.userAdd = function () {
         

            var signupDetails = {
                email: $scope.user.email,
                password: $scope.user.password,
				contact: $scope.user.phone,
                name: $scope.user.username,
                type: 'userAdd'
            };
			//console.log($scope.user.email,$scope.user.password,$scope.user.phone,$scope.user.username);
            

            $http.post($scope.http, signupDetails, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
            ).success(function (data) {
				     $state.go("app.userList");
                
            }).error(function() {
				$scope.data = "error in fetching data";
			});;
        }
}]);
