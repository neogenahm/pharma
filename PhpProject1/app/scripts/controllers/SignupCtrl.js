	
'use strict';

app.controller("SignupCtrl", ["$scope","$http", function($scope,$http) {
    //$scope.awesomeThings = ["HTML5 Boilerplate", "AngularJS", "Karma"]
	$scope.errors = [];
	$scope.signup = function () {
         

            var signupDetails = {
                email: $scope.user.email,
                password: $scope.user.password,
				contact: $scope.user.contact,
                name: $scope.user.name,
                type: 'adminSignup'
            };
			console.log($scope.user.email,$scope.user.password,$scope.user.contact,$scope.user.name);
            

            $http.post('http://localhost/IonicMiddleware/', signupDetails, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}
            ).success(function (data) {
				alert(data);
                if (data.status == 1)
                {
					
                    $state.go("app.dashboard");
                   
                } else
                {
                    $scope.errors.push(data.msg);
                }
            });
        }
}]);
