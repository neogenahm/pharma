app.directive("collapseSidebar", ["$rootScope", function() {
    return {
        restrict: "A",
        link: function(a, b) {
            var c = angular.element(".appWrapper"),
                d = angular.element(window),
                e = d.width(),
                f = function() {
                    angular.element("#sidebar").find(".ink").remove()
                },
                g = function() {
                    e = d.width(),
                    992 < e ? c.addClass("sidebar-sm") : c.removeClass("sidebar-sm sidebar-xs"), 
                    768 > e ? c.removeClass("sidebar-sm").addClass("sidebar-xs") : e < 992 ? c.removeClass("sidebar-sm sidebar-xs") : c.removeClass("sidebar-xs").addClass("sidebar-sm"), 
                    c.hasClass("sidebar-sm-forced") && c.addClass("sidebar-sm"), c.hasClass("sidebar-xs-forced") && c.addClass("sidebar-xs")
                };
            g(), d.resize(function() {
                if (e !== d.width()) {
                    var a;
                    clearTimeout(a), a = setTimeout(g, 300), f()
                }
            }), b.on("click", function(a) {
                c.hasClass("sidebar-sm") ? c.removeClass("sidebar-sm").addClass("sidebar-xs") : c.hasClass("sidebar-xs") ? c.removeClass("sidebar-xs") : c.addClass("sidebar-sm"), c.removeClass("sidebar-sm-forced sidebar-xs-forced"), c.parent().removeClass("sidebar-sm sidebar-xs"), f(), a.preventDefault()
            })
        }
    }
}]);
