'use strict';

/**
 * @ngdoc directive
 * @name minovateApp.directive:daterangepicker
 * @description
 * # daterangepicker
 */
app
  .directive('daterangepicker', function() {
    return {
      restrict: 'A',
      scope: {
        options: '=daterangepicker',
        start: '=dateBegin',
        end: '=dateEnd',
        callApplyDateFilter: '&callbackFn'
      },
      link: function(scope, element) {
          element.daterangepicker(scope.options, function(start, end) {
          scope.start = start.format('MMMM D, YYYY');
          scope.end = end.format('MMMM D, YYYY');
          scope.$apply(function(){
            localStorage.setItem('filterStartDate',scope.start);
            localStorage.setItem('filterEndDate',scope.end);
          });

          scope.callApplyDateFilter();
        });
       }
    };
  });

