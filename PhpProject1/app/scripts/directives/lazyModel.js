angular.module("lazyModel", [])

.directive("lazyModel", ["$compile", "$timeout", function(a, b) {
    return {
        restrict: "A",
        priority: 500,
        terminal: !0,
        require: ["lazyModel", "^form", "?^lazySubmit"],
        scope: !0,
        controller: ["$scope", "$element", "$attrs", "$parse", function(a, b, c, d) {
            if ("" === c.lazyModel) throw "`lazy-model` should have a value.";
            var e = d(c.lazyModel),
                f = e.assign;
            this.accept = function() {
                f(a.$parent, a.buffer)
            }, this.reset = function() {
                a.buffer = e(a.$parent)
            }, a.$watch(c.lazyModel, angular.bind(this, function() {
                this.reset()
            }))
        }],
        compile: function(c) {
            c.attr("ng-model", "buffer"), c.removeAttr("lazy-model");
            var d = a(c);
            return {
                pre: function(a) {
                    d(a)
                },
                post: function(a, c, d, e) {
                    var f = e[0],
                        g = e[1],
                        h = e[2],
                        i = h || g;
                    if (void 0 === i.$lazyControls) {
                        i.$lazyControls = [];
                        for (var j = c.parent();
                            "FORM" !== j[0].tagName;) j = j.parent();
                        j.bind("submit", function() {
                            b(function() {
                                if (g.$valid) {
                                    for (var a = 0; a < i.$lazyControls.length; a++) i.$lazyControls[a].accept();
                                    h && h.finalSubmit()
                                }
                            })
                        }), j.bind("reset", function(a) {
                            a.preventDefault(), b(function() {
                                for (var a = 0; a < i.$lazyControls.length; a++) i.$lazyControls[a].reset()
                            })
                        })
                    }
                    i.$lazyControls.push(f), a.$on("$destroy", function() {
                        for (var a = i.$lazyControls.length; a--;) i.$lazyControls[a] === f && i.$lazyControls.splice(a, 1)
                    })
                }
            }
        }
    }
}]).directive("lazySubmit", function() {
    return {
        restrict: "A",
        require: ["lazySubmit", "form"],
        controller: ["$element", "$attrs", "$scope", "$parse", function(a, b, c, d) {
            var e = b.lazySubmit ? d(b.lazySubmit) : angular.noop;
            this.finalSubmit = function() {
                e(c)
            }
        }]
    }
});
